import { test } from '@playwright/test';
import { RatingPage } from '../__page-object__/index';

test.describe('Тесты рейтинга котиков', () => {
  test('При ошибке сервера в методе rating - отображается попап ошибки', async ({ page }) => {
    const ratingPage = new RatingPage({ page: page });
    await test.step('Мокируем ответ сервера с ошибкой', async () => {
      await ratingPage.mockServerError();
    });
    await test.step('Переходим на страницу рейтинга', async () => {
      await ratingPage.gotoRatingPage();
    });
    await test.step('Проверяем отображение сообщения об ошибке', async () => {
      await ratingPage.checkForErrorMessage();
    });
  });

  test('Рейтинг котиков отображается и отсортирован по убыванию', async ({ page }) => {
    const ratingPage = new RatingPage({ page: page });
    await test.step('Переходим на страницу рейтинга', async () => {
      await ratingPage.gotoRatingPage();
    });
    await test.step('Ожидаем загрузки таблицы рейтинга', async () => {
      await ratingPage.waitForRatingTable();
    });
    await test.step('Проверяем, что рейтинг отсортирован по убыванию', async () => {
      await ratingPage.checkLikesDescendingOrder();
    });
  });
});
