import { Page, expect } from '@playwright/test';

export class RatingPage {
  readonly page: Page;
  readonly errorMessageLocator: string = 'text=Ошибка загрузки рейтинга';
  readonly ratingTableLocator: string = '.rating-names_table__Jr5Mf';
  readonly likesElementsLocator: string = '.rating-names_item-count__1LGDH.has-text-success';

  constructor({
                    page,
                }: {
        page: Page;
    }) {
        this.page = page;
    }

  async mockServerError() {
    await this.page.route('/api/likes/cats/rating', route =>
      route.fulfill({
        status: 500,
        contentType: 'application/json',
        body: JSON.stringify({ message: 'Internal Server error' }),
      }),
    );
  }

  async gotoRatingPage() {
    await this.page.goto('/rating');
  }

  async checkForErrorMessage() {
    await this.page.waitForSelector(this.errorMessageLocator);
    const errorMessage = await this.page.locator(this.errorMessageLocator).textContent();
    expect(errorMessage).toContain('Ошибка загрузки рейтинга');
  }

  async waitForRatingTable() {
    await this.page.waitForSelector(this.ratingTableLocator);
  }

  async getLikesElements() {
    return await this.page.locator(this.likesElementsLocator).elementHandles();
  }

  async checkLikesDescendingOrder() {
    const likesElements = await this.getLikesElements();
    let likes = [];
    for (const elementHandle of likesElements) {
      const likeText = await elementHandle.textContent();
      const likeCount = parseInt(likeText.replace(/\D/g, '')); // Извлекаем числовое значение
      likes.push(likeCount);
    }
    for (let i = 0; i < likes.length - 1; i++) {
      expect(likes[i]).toBeGreaterThanOrEqual(likes[i + 1]);
    }
  }
}
