import { test as base, expect } from '@playwright/test';
import { MainPage, mainPageFixture} from '../__page-object__'

const test = base.extend<{ mainPage: MainPage }>({
  mainPage: mainPageFixture,
});

test.skip('После ввода имени в строку поиска кнопка поиска - кликабельная + Page Object', async ({ page, mainPage }) => {
  await mainPage.openMainPage()
  await mainPage.inputInSearch('Петр')
  await expect(page.locator(mainPage.buttonSelector)).toBeEnabled()
});

test.skip('При поиске кота переходим на урл с его именем', async ({ page, mainPage }) => {
  const catName = 'Петр'
  await mainPage.openMainPage();
  await mainPage.inputInSearch(catName)
  await page.click(mainPage.buttonSelector)
  const currentUrl = await page.url();

  await expect(decodeURIComponent(currentUrl)).toEqual(expect.stringContaining(`search/${catName}`));
});
